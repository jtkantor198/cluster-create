# Download Requirements

./get_requirements.sh

# Install Ingress

kapp --kubeconfig $1 deploy -a haproxy-ingress -f ./haproxy-ingress

# Install Cert Manager

kapp --kubeconfig $1 deploy -a cert-manager -f ./cert-manager
