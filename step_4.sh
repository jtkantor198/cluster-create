# Step 4 Grant gitlab permissions (Need gitlab dockerconfig)
# Add ClusterRoleBindings and Registry Keys
ytt -f ./gitlab-permissions --data-value dockerconfig=$(cat docker-config.json | base64 --wrap=0) --ignore-unknown-comments | kapp -y --kubeconfig $1 deploy -a gitlab-permissions -f-
