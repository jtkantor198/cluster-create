# === haproxy-ingress ===
helm template haproxy-ingress incubator/haproxy-ingress \
--set controller.service.type=NodePort \
> ./haproxy-ingress/haproxy-ingress.yaml
# === Cert Manager ===
# Please check cert-manager.io to assure this is the latest release
curl -L  https://github.com/jetstack/cert-manager/releases/download/v0.15.0/cert-manager.yaml --output ./cert-manager/cert-manager.yml
